---
title: Without God?
date: 2021-01-01T09:51:30.421Z
description: "Ester is one of those books you've probably heard of - but read? I
  wanted to change that for myself, and then I noticed some very interesting
  things. "
---
The Book of Ester begins with Xerxes throwing a huge party.
One hundred and eighty days long.
That are 49,18033 % of the year.
So, Xerxes throws a massive party. 🎉
Xerxes ordered his servants to fetch his wife, Queen Vashti.
Vashti should show off her beauty.
When Vashti refused to come, the king became angry.
He dethroned her, and now he had to look for a new queen.
Here it gets interesting.

- - -

## The beauty contest

Xerxes needs a queen, so he sends out messengers to collect suitable women for him.
"Coincidentally" among the women is Ester - a Jewess.

So Ester comes to the king's palace and is prepared there for the competition.

For 12 months Mordechai - Ester's foster father - comes to the women's court every day to see how his daughter is doing.

Ester was Jewish, but Mordechai had told her not to tell anyone about it. Coincidentally.

- - -

## Eine Verschwörung gegen den König

One day Mordecai was sitting in the entrance of the king's palace and heard two men talking.

"I'm so annoyed that the king always gets the beautiful women, we get nothing and then we have to look after his wives, too. That's not fair! Let's get rid of the king!"

Immediately Mordechai went to Ester.

He told her what had happened and asked her to go to the king to warn him.

- - -

## The play with the sceptre

Everyone knew then that if the king did not call you - then do not come.

But Ester wanted to prevent the conspiracy.

But if the king didn't believe her, those would be her last words.

Ester overcame her doubts and went to the king.

The guard lets Esther enter and the king raises his scepter.

It's a sign that the King accepts the audience.

So: Mordechai saved the king's life through Esther. By chance.

Some time later Xerxes gave Haman the highest position in the royal court.

Haman, demanded that everyone bow when they saw him.

But Mordechai refused, he just bowed to God himself. Not to a man-made idol.

When Haman saw this, he was furious.

But just killing Mordechai was not enough for him. He wanted to kill all Jews.

- - -

## *Pur*e Hate

In the first month, the 12th year of Xerxes' reign, the dice were cast as to when the Jews were to be killed.

In Hebrew, dice means pure.

Then Haman went to the king and made him this offer:

>  If it please the king, let it be decreed that they be destroyed, and I will pay 10,000 talents of silver into the hands of those who have charge of the king’s business, that they may put it into the king’s treasuries.”
>
> [Ester 3,9](https://www.bibleserver.com/ESV/Esther3,9)

The King agrees and signs the law.

Mordechai finds out about it and asks Ester:

Go to the king, and ask him to withdraw the law.

Esther answered Mordechai, "Gather all the Jews together and fast for three days and three nights. My servants and I will fast too, and then I will go to the king."

Mordechai did everything just as Esther had told him.

\* \* * 

\## Found Grace

On the third day Esther went to the king.

He was pleased that Esther came to him and asked her what was on her mind.

Esther replied, "If it pleases the king, let the king and Haman come to the banquet I have prepared for them today."

The king hurried to send for Haman so it would happen as Esther had said. By chance.

As the king was sitting at the table with Esther and Haman, he asked, "What do you want, Esther? Everything shall be given to you. Even if it's half my kingdom."

Esther replied, "My only request is: come again tomorrow for a meal I want to prepare for the king and Haman."

On the way home, Haman sees Mordechai.

Mordechai still doesn't bow to him.

So Haman gets angry.

Haman boasts at home how Esther has invited him and the King to a meal. But Mordechai hasn't bowed to me again.

His wife suggests to him: Build a gallows, 57 meters high - and tomorrow tell the king to hang Mordecai from it.

Then you can happily go to dinner with the king.

\## Found Honour