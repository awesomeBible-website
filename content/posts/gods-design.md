---
title: "God's Design"
date: 2020-07-20T20:39:05+02:00
draft: false
author: Benjamin
feature_image: /assets/gods-design.png
---
{{< figure src="/assets/why-is-the-sky-blue.jpg" title="How are northern lights created?" >}}

{{< figure src="/assets/photo-of-mountain-under-cloudy-sky-4101555.jpg" title="Why is the sky blue?" >}}

{{< figure src="/assets/pexels-photo-1680140.jpeg" title="Why does water shimmer blue?" >}}
* * *

If you want answers to the questions, you are here in wrong place.
Of course not if you want to know why the sky is blue.
Because I've explained that here:

{{< figure src="/assets/why-is-the-sky-blue.jpg" title="Why is the Sky blue?" link="https://en.awesomebible.de/blog/why-is-the-sky-blue/" >}}

And if you don't want to answer these questions - you certainly want me to get to the point. *And I'm going to get to the point.*

## God's Design

![Green Stripe](/assets/stripe_aaf0d1.png)
![Green Stripe](/assets/stripe_3cb371.png)
![Green Stripe](/assets/stripe_2e8b57.png)
![Green Stripe](/assets/stripe_556b2f.png)

Different Greenshades - and now? 🤔

"The world is colorless." [ [https://en.awesomebible.de/why-is-the-sky-blue/](https://en.awesomebible.de/why-is-the-sky-blue/) ]

We see colors. Why? Because light enters our eyes.

The world is colorless, but nevertheless we can see the 4 stripes above.

{{< video label="Northern Lights" mp4="/assets/Northern_Lights-2267.mp4" >}}
## Chapter 1: How are northern lights created?

The sun shoots electrons and protons onto the earth.
At the magnetic field the electrons separate from the protons - because... laws of nature!
But they want to meet again, and they do so by the shortest route at the North Pole.
When the electrons collide with the protons, light is produced.

That's so awesome! And so beautiful! :heart:

{{< video label="Blue Sky" mp4="/assets/Clouds-3723.mp4" >}}
## Chapter 2: Why is the Sky blue?

The *white* light from the sun shines onto the earth.

White light contains all colors.

The wavelengths of blue light rays have a higher probability of hitting dust, which catapults the blue light back.

This way most of the blue light stays up and we see the sky blue.

The principle is so simple: light meets dust - the result is so ingenious.

{{< video label="Gods Design in the Bible" mp4="/assets/Book_Video-2268807.mp4" >}}
## Chapter 3: God's Design in the Bible

The bible has many different styles in which it is written.
The video from the Bible Project gives a really good overview about that.

{{< youtube oUXJ8Owes8E >}}

The bible isn't *only a book*. The bible is God's word.

But Benjamin - I know! What is so special about that?


![Green Stripe](/assets/stripe_aaf0d1.png)
![Green Stripe](/assets/stripe_3cb371.png)
![Green Stripe](/assets/stripe_2e8b57.png)
![Green Stripe](/assets/stripe_556b2f.png)

What is so special about these four stripes? There all green!

Every Bibleauthor has a different Approach, a different sight on the world. And everytime you open the bible, a different part of you is spoken to.

It's all green, but I wouldn't paint my room with the last green stripe.

## So, kids, what you can learn from it this time: Don't paint your room in poisonous green or how the cool kids would say: #556b2f!

But for real: Ever since I wrote the text ["Why is the sky blue?"](https://en.awesomebible.de/blog/why-is-the-sky-blue/), I can't stop thinking about how awesome it all is!

