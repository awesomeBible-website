---
title: "Why is the Sky blue?"
date: 2020-07-20T17:23:06+02:00
draft: false
author: Benjamin
feature_image: "/assets/why-is-the-sky-blue.jpg"
---

So seemingly simple things are so complex.

Actually is the world colorless.

## But why? 🤔
## Why did God create such a beautiful world which then has no colour?

Not exactly - the objects around you are indeed colorless but that's more awesome than you think.

The light of the sun ☀️ - which by the way is white, not yellow - shines on the objects, and they obsorb it -  so swallow all shades that are not needed, to display the sepecial Colorshade that the object has.

For example, the color of an object can be used to determine some of its properties.

Since I know this, I am only more amazed about the colourless and yet colourful world that God has created.

And now the question you clicked on this article for...

## Why is the sky blue?

The sky is blue, because subeams collide with dust.

The light has different wavelengths, blue light has very short waves.
Because of the short wavelength that blue light has, is is more likely for blue light to collide with dust particles in the atmosphere.
This collision causes the light to be catapulted to all directions - mostly up.
Because of that we see the blue sky. 🌌

So I think it's about time to come to a conclusion.

## Conclusion?
I find it so interesting how creative and complex God has thought up this world.
Everything works together.
And even if some things seem simple, they usually are not, but carefully thought out and created.

Even if some say we are "coincidence",
I can only say that if we are coincidence... 
**Then who put those grains of dust in the atmosphere?**
