---
title: "About us" 
date: "2020-01-01" 
aliases: ["about-us","contact"] 
author: "Benjamin"
---
Hello.
You are probably wondering: What is awesomeBible?
Let the story begin! 🤠️

## First try aka. Bibel40000
On [February 11th 2016](https://bibel40000.blogspot.com/2016/) I started Bibel40000.
I had no clear goal. I just wanted to spread the word of god - tell everyone about it.
And I wanted to have a place where I could write down my thoughts.

## Second try aka. Bibel4000site
In the same year, I started the transition to [Bibel4000site](https://bibel4000site.wordpress.com).
The slogan for Bibel4000site was "Die Bibel. für alle. erklärt!" meaning "The Bible. explained. for everyone."

Then Bibel4000site came to halt.
I posted less and less content and I somehow lost the interest in Bibel4000site.
The reasons where pretty simple: 
- The site displayed ads because it was hosted on a free hosting service.
- I ran slowly out of storage space.
- I hadn't a "real" domain. Only a WordPress.com-Subdomain.

## Third try aka. awesomeBible
In January 2020, I finally moved to [awesomeBible.de](https://awesomeBible.de).
- No ads.
- More than enough storage space.
- A "real" domain: awesomeBible.de

This time the slogan is

> Wir wollen Menschen von Gottes genialem Plan begeistern,
> deshalb erklären wir die Bibel einfach,
> verständlich und für alle zugänglich!

which means: 

> We want to inspire people with God's ingenious plan, 
> so we explain the Bible in a simple, 
> understandable and accessible way for everyone!

## Try 3b aka. awesomeBible English
In July 2020 we started an experiment.
Translate all content on awesomeBible into English (and see what happens.)
With that we wanted to expand our reach and try to stay true to our slogan.

> ... understandable and accessible for everyone!

You don't have to translate it to English first to be able to read it. We did that for you.

* * *
## Contact us

{{< form slug="contact" captcha="true" >}}
